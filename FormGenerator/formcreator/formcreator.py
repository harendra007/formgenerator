'''
Created on Feb 9, 2020

@author: Harendra
'''
from formdata import FormData
import json
from jinja2 import Environment, BaseLoader



class Form():
    def __init__(self,filename):
        self.filename=filename
        self.form=None
        self.parse_userfile()
    
    def set_options(self,formelem):
        if formelem.options==None:
            return 
        
        if formelem.type!="dropdownrange":
            return
        
        options=[]
        for option in formelem.options:
            options.append(list(range(int(option[0]),int(option[1])+1)))
        
        if(len(options))==1:
            options=options[0]
        
        formelem.options=options
    
    def assign_field(self,form_definition):
        form=FormData()
        form.fieldname=form_definition["fieldname"]
        attributes=form_definition["attributes"]
        
        for key in attributes:
            value=attributes[key]
            if key=="visible":
                form.visible=value
            if key=="type":
                form.type=value
            if key=="required":
                form.required=value
            if key=="label":
                form.label=value
            if key=="html_text":
                form.label=value
            if key=="html_type":
                form.label=value
            if key=="html_id":
                form.html_id=value
            if key=="placeholder_text":
                form.placeholder_text=value
            if key=="error_id":
                form.error_id=value
            if key=="error_message":
                form.error_message=value
            if key=="options":
                form.options=value
                self.set_options(form)
        return form
            
    
    def parse_userfile(self):
        text=open(self.filename).read()
        form_definition=json.loads(text)
        self.form=[]
        for fieldname in form_definition:
            form=self.assign_field(form_definition[fieldname])
            self.form.append(form)
        
    

class HtmlRenderer():
    def __init__(self,templatefile,context):
        self.templatefile=templatefile
        self.context=context
    
    def render(self):
        text=open(self.templatefile).read()
        rtemplate = Environment(loader=BaseLoader).from_string(text)
        generated_html = rtemplate.render(**self.context)
        return generated_html


class JavascriptCreator():
    pass





class FormHtmlCreator():
    
    def __init__(self,form_item):
        self.form_item=form_item
        self.create_form_html()
        self.text_template=r"C:\Users\Harendra\git\formgenerator\templates\formelem_textfield.html"
        self.option_template=r"C:\Users\Harendra\git\formgenerator\templates\formelem_select.html"

    def create_textbox(self):
        context=self.form_item.__dict__
        text_template=r"C:\Users\Harendra\git\formgenerator\templates\formelem_textfield.html"
        renderer=HtmlRenderer(text_template,context)
        rendered_html=renderer.render()
        self.form_item.html_text=rendered_html
    
    def create_option_box(self):
        context=self.form_item.__dict__
        option_template=r"C:\Users\Harendra\git\formgenerator\templates\formelem_select.html"
        renderer=HtmlRenderer(option_template,context)
        rendered_html=renderer.render()
        self.form_item.html_text=rendered_html

    def create_option_box_range(self):
        context=self.form_item.__dict__
        option_template=r"C:\Users\Harendra\git\formgenerator\templates\formelem_select.html"
        option_template_multi=r"C:\Users\Harendra\git\formgenerator\templates\formelem_select_multi.html"
        if len(self.form_item.options)==2:
            option_template=option_template_multi
        renderer=HtmlRenderer(option_template,context)
        rendered_html=renderer.render()
        self.form_item.html_text=rendered_html


    def create_select_box(self):
        context=self.form_item.__dict__
        option_template=r"C:\Users\Harendra\git\formgenerator\templates\form_multi_check_box.html"
        renderer=HtmlRenderer(option_template,context)
        rendered_html=renderer.render()
        self.form_item.html_text=rendered_html

    def create_radio_box(self):
        context=self.form_item.__dict__
        option_template=r"C:\Users\Harendra\git\formgenerator\templates\form_radio.html"
        renderer=HtmlRenderer(option_template,context)
        rendered_html=renderer.render()
        self.form_item.html_text=rendered_html        
                
    def create_form_html(self):
        if self.form_item.type=="text":
            self.create_textbox()
        if self.form_item.type=="dropdown":
            self.create_option_box()
        if self.form_item.type=="dropdownrange":
            self.create_option_box_range()
        if self.form_item.type=="choice":
            self.create_radio_box()
        if self.form_item.type=="multichoice":
            self.create_select_box()
        

        
class FormCreator():
    
    def __init__(self,filename):
        self.filename=filename
        
    def create_form(self):
        form=Form(self.filename).form
        for form_item in form:
            if form_item.visible=="false":
                continue
            FormHtmlCreator(form_item)
            print(form_item.html_text)

filename=r"C:\Users\Harendra\git\formgenerator\input\RestaurantModel.json"
f=FormCreator(filename)
f.create_form()

        
            
        


