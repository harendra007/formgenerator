'''
Created on 2014/01/14

@author: harendra
'''
import jinja2
import os

class FormCreator():
    def __init__(self,data):
        self.data=data
        self.inputdirectory='C:\\Users\\harendra\\git\\formgenerator\\FormGenerator'
        self.createForms()
    
    def renderTemplate(self,context,filename):
        t=jinja2.Template(open(filename,'r').read())
        print t.render(context)
    
    def createWebForm(self,elems):
        webfile=os.path.join(self.inputdirectory,"webform.html")
        self.renderTemplate({'labels':elems}, webfile)
    
    def createDataHandler(self,elems):
        filename=os.path.join(self.inputdirectory,'DataHandler.py')
        self.renderTemplate({'putdata':elems,'getdata':elems},filename)
    
    def createInputHandler(self,elems):
        filename=os.path.join(self.inputdirectory,'inputhandler.py')
        self.renderTemplate({'labels':elems},filename)
    
    def createModel(self,elems):
        filename=os.path.join(self.inputdirectory,'models.py')
        self.renderTemplate({'labels':elems}, filename)
            
    def createForms(self):
        inputelems=[]
        for elem in self.data:
            inputelems.append(elem['datakey'])
        self.createDataHandler(inputelems)
        self.createWebForm(inputelems)
        self.createInputHandler(inputelems)
        self.createModel(inputelems)

data=[{'datakey':'paymenttype','datatype':'string'},
      {'datakey':'paymentduration','datatype':'string'},
      {'datakey':'clientnumber','datatype':'string'},
      {'datakey':'billingtype','datatype':'string'},
      {'datakey':'createddate','datatype':'datetime'},
    ]
FormCreator(data)


    
    