class DataHandler():
    def handlePut(self,entid,type,params):
        if entid==None:
            entid==Uniquegetter.uid()
        entity=self.getEntity()
    
    def putData(self,entity,inputdata):
{% for putelem in putdata %}
        if inputdata.has_key('{{putelem}}'):
            entity.{{putelem}}=inputdata['{{putelem}}']
{% endfor %}
        self.writeToDataStore(entity)
    
    def getData(self,entity,getparams):
        output={}
        for param in getparams:
{% for getelem in getdata %}
            if param=='{{getelem}}':
                output['{{getelem}}']=entity.{{getelem}}
{% endfor %}
        return output
