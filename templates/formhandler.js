class {{objectclass}} {
	
	constructor(){
		{% for {{textbox}} in formelem.textboxes %}
		this.{{textbox.fieldname}}=null;
		{% endfor %}
	}
}




function {{formhandler}} {
	
	
	this.initialize=function(){
		
		startformaction();
		
		
	}
	
	
	function dataaction(){
		
		var data=new {{objectclass}}();
		//handle text boxes
		
		var error=false;
		
		{% for {{textbox}} in formelem.textboxes %}
		
			var {{textbox.fieldname}}=$("#{{textbox.id}}").val();
			if({{textbox.fieldname}}.length==0){
				error=true;
				$("#{{textbox.error_id}}").text({{textbox.error_text}});
			}
			data.{{textbox.fieldname}}={{textbox.fieldname}};
		
		{% endfor %}
		
		if(error==true){
			return null;
		}
		
		return data;
		
		
	}
	
	
	function disableButton(button){
		$('#'+button).addClass("disabled");
	}
	
	
	function enableButton(button){
		$("#"+button.removeClass("disabled"));
	}
	
	
	
	
	function startformaction(){
		
		
		$("#{{actionid}}").click(function(){
		
			var data=gatherdata();
			if (data==null){
				return;
			}
			
			//disable send button enable
			disableButton("sendbutton");
			enableButton("waitingbutton");
			
			$.ajax(){
				
				data:JSON.stringify(data),
				success:function(response){
					if(response.result=="error"){
						
						disableButton("waitingbutton");
						enableButton("sendbutton");
						
						return;
						
					}
					
					windows.location="";
				},	
			}
			
			var url="";
			$()
			
		})
		
		
		
	}
	
}